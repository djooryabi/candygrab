package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.mygdx.game.screens.GameScreen;

/**
 * Created by Daniel on 6/17/2015.
 */
public class MyInputProcessor implements InputProcessor
{

    GameScreen game;

    public MyInputProcessor(GameScreen game)
    {
        this.game = game;
    }
    @Override
    public boolean keyDown(int keycode)
    {
        if (keycode == Input.Keys.P)
        {
            game.songs.get(game.currentSongIdx).pause();
            game.logger.debug("song paused");
        }
        else if (keycode == Input.Keys.R)
        {
            game.songs.get(game.currentSongIdx).play();
            game.logger.debug("Song resumed");
        }
        else if (keycode == Input.Keys.UP)
        {
            game.changeVolume(game.VOLUME_CHANGE);
            game.logger.debug("volume up");
        }
        else if (keycode == Input.Keys.DOWN)
        {
            game.changeVolume(-game.VOLUME_CHANGE);
            game.logger.debug("volume down");
        }
        else if (keycode == Input.Keys.RIGHT)
        {
            game.playSong((game.currentSongIdx + 1) % game.songs.size);
            Gdx.app.log(" MusicSample", "Next song");
        }
        else if (keycode == Input.Keys.LEFT)
        {
            int songIdx = (game.currentSongIdx - 1) < 0 ? game.songs.size - 1 : game.currentSongIdx - 1;
            game.playSong(songIdx);
            Gdx.app.log(" MusicSample", "Previous song");
        }


        return true;
    }


    @Override
    public boolean keyUp(int keycode)
    {
        return false;
    }

    @Override
    public boolean keyTyped(char character)
    {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        if (game.plateBoundingBox.contains((float) screenX, (float) (game.HEIGHT - screenY)))
        {

            game.platePosition.set(screenX - 250.0f, (game.HEIGHT - screenY) - 250.0f);
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY)
    {
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        return false;
    }
}
