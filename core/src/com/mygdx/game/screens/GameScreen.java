package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Logger;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Daniel on 6/17/2015.
 */
public class GameScreen extends ScreenAdapter
{
    public SpriteBatch batch;
    public static float WIDTH, HEIGHT;
    public Texture img1, img2, img3, img4, backgroundTexture, candyfonts;
    public Vector2 platePosition;
    public Rectangle plateBoundingBox, plateCollisionRect;
    public static final float VOLUME_CHANGE = 0.2f;
    public Sprite backgroundSprite;
    public SpriteBatch backgroundSpriteBatch;
    public Array<Music> songs;
    public int currentSongIdx;
    public float volume;
    public SongListener listener;
    public static float CANDYWIDTH;
    public static final float PLATEWIDTH = 500.0f;
    public static final float PLATEHEIGHT = 250.0f;
    public static int level = 1;
    public Logger logger = new Logger("game", Logger.INFO);
    public ArrayList<Candy> candies = new ArrayList<Candy>();
    public BitmapFont font;
    public int score;
    public IntMap<Sound> sfx;
    public Timer timer = new Timer();
    public ListIterator listIterator = candies.listIterator();
    public MyInputProcessor myInputProcessor;

    TimerTask timerTask = new TimerTask()
    {
        public void run()
        {
            synchronized (candies)
            {
                candies.add(new Candy());
            }
        }


    };

    private class SongListener implements Music.OnCompletionListener
    {
        @Override
        public void onCompletion(Music music)
        {
            playSong((currentSongIdx + 1) % songs.size);
            logger.debug("Song finished, playing next song");
        }


    }

    @Override
    public void show()
    {
        myInputProcessor = new MyInputProcessor(this);
        sfx = new IntMap<Sound>();
        sfx.put(1, Gdx.audio.newSound(Gdx.files.internal("music/ring.wav")));
        sfx.put(2, Gdx.audio.newSound(Gdx.files.internal("music/levelup.wav")));

        listener = new SongListener()
        {
            @Override
            public void onCompletion(Music music)
            {
                playSong((currentSongIdx + 1) % songs.size);
                Gdx.app.log(" MusicSample", "Song finished, play next song");
            }
        };
        songs = new Array<Music>();
        songs.add(Gdx.audio.newMusic(Gdx.files.internal("music/candymusic.mp3")));
        currentSongIdx = 0;
        volume = 1.0f;
        Gdx.input.setInputProcessor(myInputProcessor);
        playSong(0);

        font = new BitmapFont(Gdx.files.internal("candyfonts.fnt"));
        backgroundSpriteBatch = new SpriteBatch();
        WIDTH = Gdx.graphics.getWidth();
        HEIGHT = Gdx.graphics.getHeight();
        CANDYWIDTH = HEIGHT / 6.0f;
        platePosition = new Vector2(WIDTH / 2.0f, HEIGHT / 2.0f);
        timer.scheduleAtFixedRate(timerTask, 1000, 2000);

        plateBoundingBox = new Rectangle(platePosition.x, platePosition.y, PLATEWIDTH, PLATEWIDTH);
        batch = new SpriteBatch();
        candyfonts = new Texture("candyfonts.png");
        img1 = new Texture("candy1.png");
        img2 = new Texture("candy2.png");
        img3 = new Texture("candy3.png");
        img4 = new Texture("plate.png");
        backgroundTexture = new Texture("background.jpeg");
        backgroundSprite = new Sprite(backgroundTexture);
        logger.info("loaded all textures");
    }

    @Override
    public void dispose()
    {
        super.dispose();
        for (Music song : songs)
        {
            song.dispose();
        }

        img1.dispose();
        img2.dispose();
        img3.dispose();
        img4.dispose();
        backgroundTexture.dispose();
        candyfonts.dispose();
        font.dispose();

    }

    @Override
    public void render(float delta)
    {
        plateCollisionRect = new Rectangle(platePosition.x + PLATEWIDTH / 3.0f, platePosition.y, PLATEWIDTH / 3.0f, PLATEHEIGHT);


        // check if candies went off the screen
        synchronized (candies)
        {
            listIterator = candies.listIterator();

            while (listIterator.hasNext())
            {
                Candy c = (Candy) listIterator.next();
                c.update(Gdx.graphics.getDeltaTime());

                if (c.getY() <= -CANDYWIDTH)
                {
                    listIterator.remove();
                }
                else if (c.isTouchingRectangle(plateCollisionRect))
                {
                    listIterator.remove();
                    logger.info("detected and removed candy");
                    score += 10;
                    sfx.get(1).play();

                    int prevLevel = level;
                    level = (int) (1 + 0.1 * Math.sqrt((double) score));

                    if (level > prevLevel)
                    {
                        sfx.get(2).play();
                    }

                }
            }

        }


        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        plateBoundingBox.set(platePosition.x, platePosition.y, 500.0f, 500.0f);

        backgroundSpriteBatch.begin();

        backgroundSpriteBatch.draw(backgroundTexture, 0.0f, 0.0f, WIDTH, HEIGHT);

        backgroundSpriteBatch.end();


        batch.begin();
        batch.draw(img4, platePosition.x, platePosition.y, 500.0f, 500.0f);

        synchronized (candies)
        {
            for (Candy c : candies)
            {
                switch (c.getType())
                {
                    case 1:
                        batch.draw(img1, c.getX(), c.getY(), CANDYWIDTH, CANDYWIDTH);
                        break;
                    case 2:

                        batch.draw(img2, c.getX(), c.getY(), CANDYWIDTH, CANDYWIDTH);

                        break;
                    case 3:

                        batch.draw(img3, c.getX(), c.getY(), CANDYWIDTH, CANDYWIDTH);
                        break;

                }
            }
        }

        batch.end();

        batch.begin();
        font.draw(batch, Integer.toString(score), 0.9f * WIDTH, HEIGHT);
        font.draw(batch, "Level: " + Integer.toString(level), 0.01f * WIDTH, HEIGHT);
        batch.end();
    }

    public void playSong(int songIdx)
    {
        Music song = songs.get(currentSongIdx);
        song.setOnCompletionListener(null);
        song.stop();

        currentSongIdx = songIdx;
        song = songs.get(currentSongIdx);
        song.play();
        song.setVolume(volume);
        song.setOnCompletionListener(listener);
    }

    public void changeVolume(float volumeChange)
    {
        Music song = songs.get(currentSongIdx);
        volume = MathUtils.clamp(song.getVolume() + volumeChange, 0.0f, 1.0f);
        song.setVolume(volume);
    }
}
