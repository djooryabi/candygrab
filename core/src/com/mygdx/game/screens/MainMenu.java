package com.mygdx.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Daniel on 6/17/2015.
 */
public class MainMenu extends ScreenAdapter
{
    private final GameScreen gameScreen;
    private final Game myGame;
    private Stage stage;
    private Image gameTitle;
    private TextButton btnPlay, btnExit;
    private BitmapFont font;
    private Table table;
    private Texture gameTitleTex, buttonUpTex, buttonOverTex, buttonDownTex;
    private TextButton.TextButtonStyle tbs = new TextButton.TextButtonStyle();


    public MainMenu(Game myGame, GameScreen gameScreen)
    {

        this.myGame = myGame;
        this.gameScreen = gameScreen;
        gameTitleTex = new Texture(Gdx.files.internal("title.png"));
        gameTitle = new Image(new TextureRegionDrawable(new TextureRegion(gameTitleTex)));
        buttonUpTex = new Texture(Gdx.files.internal("myactor.png"));
        buttonOverTex = new Texture(Gdx.files.internal("myactorOver.png"));
        buttonDownTex = new Texture(Gdx.files.internal("myactorDown.png"));

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        font = new BitmapFont(Gdx.files.internal("candyfonts.fnt"));
        tbs.font = font;
        tbs.up = new TextureRegionDrawable(new TextureRegion(buttonUpTex));
        tbs.over = new TextureRegionDrawable(new TextureRegion(buttonOverTex));
        tbs.down = new TextureRegionDrawable(new TextureRegion(buttonDownTex));
        btnPlay = new TextButton("PLAY", tbs);
        btnExit = new TextButton("Exit", tbs);
        btnPlay.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                Gdx.app.log("game", "PLAY");
                MainMenu.this.myGame.setScreen(MainMenu.this.gameScreen);
            }
        });

        btnExit.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                Gdx.app.log("game", "EXIT");
                Gdx.app.exit();
            }
        });

        table = new Table();
        table.row();
        table.add(gameTitle).padTop(10f).colspan(2);
        table.row();
        table.add(btnPlay).padTop(10f).colspan(2);
        table.row();
        table.add(btnExit).padTop(10f).colspan(2);
        table.row();
        table.setFillParent(true);
        table.pack();
        table.getColor().a = 0f;
        table.addAction(Actions.fadeIn(2f));
        stage.addActor(table);
    }

    @Override
    public void dispose()
    {
        super.dispose();
        stage.dispose();
        font.dispose();
        gameTitleTex.dispose();
        buttonUpTex.dispose();
        buttonOverTex.dispose();
        buttonDownTex.dispose();


    }


    @Override
    public void render(float delta)
    {
        super.render(delta);
        Gdx.graphics.getGL20().glClearColor(255 / 255.0f, 182 / 255.0f, 193 / 255.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }
}
