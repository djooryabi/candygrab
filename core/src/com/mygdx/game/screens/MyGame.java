package com.mygdx.game.screens;

import com.badlogic.gdx.Game;

/**
 * Created by Daniel on 6/17/2015.
 */
public class MyGame extends Game
{
    MainMenu mainMenu;
    GameScreen gameScreen;

    @Override
    public void create()
    {
        gameScreen = new GameScreen();
        mainMenu = new MainMenu(this, gameScreen);
        setScreen(mainMenu);
    }

    @Override
    public void dispose()
    {
        super.dispose();
        if(mainMenu != null)
            mainMenu.dispose();
        if(gameScreen.myInputProcessor != null)
            gameScreen.dispose();
    }
}
