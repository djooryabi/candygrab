package com.mygdx.game.screens;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Daniel on 6/13/2015.
 */
public class Candy
{
    private int type;

    private Vector2 position = new Vector2();
    private Rectangle candyRect = new Rectangle();
    private Vector2 velocity = new Vector2(0.0f, -(2.0f* GameScreen.HEIGHT + GameScreen.HEIGHT*(GameScreen.level*0.4f))/5.0f);

    public Candy(int type)
    {
        this.type = type;
        position.set(MathUtils.random(0.0f, GameScreen.WIDTH), GameScreen.HEIGHT);
        candyRect.set(getX(), getY(), GameScreen.CANDYWIDTH, GameScreen.CANDYWIDTH);
    }

    public Candy()
    {
        this.type = MathUtils.random(1, 3);
        position.set(MathUtils.random(0.0f, GameScreen.WIDTH), GameScreen.HEIGHT);
        candyRect.set(getX(), getY(), GameScreen.CANDYWIDTH, GameScreen.CANDYWIDTH);
    }

    public void update(float delta)
    {
        position.add(velocity.cpy().scl(delta));
        candyRect.set(getX(), getY(), GameScreen.CANDYWIDTH, GameScreen.CANDYWIDTH);
    }

    public float getX()
    {
        return position.x;
    }

    public float getY()
    {
        return position.y;
    }

    public void setX(float x)
    {
        position.set(x, position.y);
    }

    public void setY(float y)
    {
        position.set(position.x, y);
    }

    public int getType()
    {
        return type;
    }

    public boolean isTouchingRectangle(Rectangle rect)
    {
        return Intersector.overlaps(this.candyRect, rect);
    }

}
